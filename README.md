# twister

## Overview
WebServer for broadway2-API based on python tornado.
First draft will be demonstrator to send/receive Raw-Ethernet Frames via Tornado Async-WebSockets to browser running javascript rendering GUI.

There are 2 async web-sockets: One for Layer-2 Rx (Receive eXchange) and one for Layer-2 Tx (Transmit eXchange).

 - Received data from broadway2-API Raw-Rx will be moved via async websocket to receive-path and
 - sending websocket data to transmit-path, webserver will send this data to broadway2-API Raw-Tx.

## Broadway2-API
Using USB-100Base-T1 Adapter FC611/FC612 (the red stick) enables direct access to Ethernet Layer-2 frames. 
Demonstrator will receive Layer2-Frames coming from second adapter as Sensor-Dummy.


## Goals

### Demonstrator (Prototype)
For easier testing and verfication, tornado based webserver will come with build-in loop to disconnect broadway2-API and run in test-mode.

There is no protocol used in first sample, but one option could be to implement a standard protocol layer from the IoT-world later.
As a reference for receiving data, refer to python sample `raw_receive.py`.

### Webserver based on Tornado with JS-Frontend
Raspberry Pi using FC611/FC612, broadway2Api installed and Python 3.

* Web-Server with [Tornado](https://www.tornadoweb.org/en/stable/) installed
	* List of connected sticks in order to select one
	* Echo-Mode as Fallback (no sticks connected): Loop input through server and output again
* JS Frontend that opens Websocket connection to webserver and outputs data in DOM
